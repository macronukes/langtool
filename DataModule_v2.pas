unit DataModule_v2;

interface

uses
  System.SysUtils, System.Classes, System.JSON, System.UITypes, dbCrud, FMX.Dialogs, FMX.Forms, REST.Client,
  REST.Types, Data.Bind.Components, Data.Bind.ObjectScope, FMX.Types, System.Threading, FMX.StdCtrls
  , FMX.Controls, FMX.Filter.Effects, FMX.Graphics, FMX.Utils, FMX.Ani
  ;

type
  TIndicator = class(TAniIndicator)
  private
    FIndicator: TAniIndicator;
    FVisible: Boolean;
    procedure SetIndicator(AIndicator: TAniIndicator);
  public
    Constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
    property Indicator: TAniIndicator read FIndicator write SetIndicator;
  end;

type
  TMainTransClient = class(TTransClient, ITransClient)
  private
    function Read( priority: boolean = false; strIn: string = ''; langOut: string = ''; ATask: TATask_Trans = nil ): string;
    //function Inse( priority: boolean = false; strIn: string = ''; langOut: string = ''; ATask: TATask_Trans = nil ): string;
  end;

type
  TCreateTransEndpoint = function( BaseURL_Trans: string; ApiName_Trans: string; ApiKey_Trans: string ): ITransClient of object;
  THandleTransReadStart = procedure( sender: ITransClient ) of object;
  THandleTransReadSuccess = procedure( sender: ITransClient; Results: string ) of object;
  THandleTransReadFail = procedure( sender: ITransClient; Error: string ) of object;

type
  TdbModule2 = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FCreateTransEndpoint: TCreateTransEndpoint;
    FHandleTransReadStart: THandleTransReadStart;
    FHandleTransReadSuccess: THandleTransReadSuccess;
    FHandleTransReadFail: THandleTransReadFail;

    Indicator: TIndicator;

    function CreateTransEndpoint( BaseURL_Trans: string; ApiName_Trans: string; ApiKey_Trans: string ): ITransClient;
    procedure HandleTransReadStart( sender: ITransClient );
    procedure HandleTransReadSuccess( sender: ITransClient; Results: string );
    procedure HandleTransReadFail( sender: ITransClient; Error: string );

  public
    { Public declarations }
    FTransClient: ITransClient; //ICrudClient;
    procedure AnimationStarts;
    procedure AnimationEnds;

    property OnCreateTransEndpoint: TCreateTransEndpoint read FCreateTransEndpoint write FCreateTransEndpoint;
    property OnHandleTransReadStart: THandleTransReadStart read FHandleTransReadStart write FHandleTransReadStart;
    property OnHandleTransReadSuccess: THandleTransReadSuccess read FHandleTransReadSuccess write FHandleTransReadSuccess;
    property OnHandleTransReadFail: THandleTransReadFail read FHandleTransReadFail write FHandleTransReadFail;

  end;

var
  dbModule2: TdbModule2;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses
  fTLangTool
  ;

{$R *.dfm}

procedure TIndicator.SetIndicator(AIndicator: TAniIndicator);
begin
  if FIndicator <> AIndicator then
  begin
    FIndicator := AIndicator;
  end;

  FIndicator.Visible:=False;
  FIndicator.Enabled:=True;
end;

constructor TIndicator.Create(AOwner: TComponent);
var
  bla: TBitmapListAnimation;
  effect: TFillRGBEffect;

  procedure ReplaceColor(const ABitmap: TBitmap);
  var
    I, J: Integer;
    M: TBitmapData;
    C: PAlphaColorRec;
  begin
    if ABitmap.Map(TMapAccess.ReadWrite, M) then
    try
      for J := 0 to ABitmap.Height - 1 do
        for I := 0 to ABitmap.Width - 1 do
        begin
          C := @PAlphaColorArray(M.Data)[J * (M.Pitch div 4) + I];
          //if C^.Color = TAlphaColorRec.Black then
            C^.Color := TAlphaColorRec.Red;
        end;
    finally
      ABitmap.Unmap(M);
    end;
  end;

begin
  inherited Create(AOwner);

  SetSubComponent(true);
  Align:=TAlignLayout.Center;

  //bla := FindStyleResource('ani') as TBitmapListAnimation;
  //ReplaceColor(bla.AnimationBitmap);
  //if not Assigned(effect) then
  effect := TFillRGBEffect.Create(self);
  effect.Color:=TAlphaColorRec.Red;
  Self.AddObject(effect);

  TControl(AOwner).AddObject(Self);
  BringToFront;

end;

function TMainTransClient.Read( priority: boolean; strIn, langOut: string; ATask: TAtask_Trans): string;
var
  {$IFDEF ANDROID}
  //NS: TNetworkState;
  {$ENDIF}
  Client: TTransClient;
begin

  if langOut<>'es' then
  begin
    {$IFDEF ANDROID}
    //NS := TNetworkState.Create;
    //if NS.IsConnected('Sin conexion a internet. No se recupera ning�n dato') then
    {$ENDIF}
      Result:=Client.Read(priority, strIn, langOut, ATask);
      //dbModule2.FMainClient.Read( priority, query, filters, encrypt, descrypt, atask);
  end else
    Result:=strIn;
end;

procedure TdbModule2.DataModuleCreate(Sender: TObject);
begin
  OnCreateTransEndpoint := CreateTransEndpoint;
  OnHandleTransReadStart := HandleTransReadStart;
  OnHandleTransReadSuccess := HandleTransReadSuccess;
  OnHandleTransReadFail := HandleTransReadFail;

  //IdOpenSSLSetLibPath(TPath.GetDocumentsPath);

  FTransClient := OnCreateTransEndPoint(  cBaseURL_Trans, cApiName_Trans, cApiKey_Trans );
end;

procedure TdbModule2.AnimationEnds;
begin
    Indicator.Visible:=false;
    Indicator.Enabled:=False;
    Indicator.SendToBack;
end;

procedure TdbModule2.AnimationStarts;
begin
    if not Assigned(Indicator) then
      Indicator := TIndicator.Create(FormTLangTool);

    Indicator.Enabled:=True;
    Indicator.BringToFront;
    Indicator.Visible:=true;
end;

procedure TdbModule2.HandleTransReadSuccess(sender: ITransClient; Results: string);
begin
  AnimationEnds;
end;

function TdbModule2.CreateTransEndpoint( BaseURL_Trans, ApiName_Trans, ApiKey_Trans: string ): ITransClient;
begin
  Result := CreateTrans(BaseURL_Trans, ApiName_Trans, ApiKey_Trans);
  Result.OnTransReadStart := HandleTransReadStart;
  Result.OnTransReadSuccess := HandleTransReadSuccess;
  Result.OnTransReadFail := HandleTransReadFail;
end;

procedure TdbModule2.HandleTransReadStart(sender: ITransClient);
begin
  AnimationStarts;
end;

procedure TdbModule2.HandleTransReadFail(sender: ITransClient; Error: string);
begin
    AnimationEnds;
    ShowMessage(Error+' Cerrando.');
    //Application.Terminate;
end;

end.

