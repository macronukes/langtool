unit dbCrud;

interface
uses
  System.Classes,
  sysutils,
  StrUtils,
  System.Threading,
  system.json,
  REST.client,
  REST.Types,
  System.Generics.Collections
  , System.Generics.Defaults
  //, IdBaseComponent, IdComponent,
  //IdTCPConnection, IdTCPClient, IdHTTP,
  //, idURI //, IdSSLOpenSSL
  , System.Net.URLClient, System.Net.HttpClient,
  System.Net.HttpClientComponent
  //, System.NetEncoding
  ;

const
  cBaseURL_Trans = 'https://mydomain.cognitiveservices.azure.com/translator/text/v3.0/translate?api-version=3.0&to=';
  cApiName_Trans = 'Ocp-Apim-Subscription-Key';
  cApiKey_Trans = 'MyAPI_KEY';

var
  T: array of ITask;

type
  ITransClient = interface;

  TAtask_Trans = reference to procedure(fResults: string='');

  TTransStartEvent = procedure ( sender: ITransClient ) of object;
  TTransSuccessEvent = procedure ( sender: ITransClient; Results: string ) of object;
  TTransFailEvent = procedure( sender: ITransClient; Error: string ) of object;

  ITransClient = interface['{DAA0F591-41C9-44E2-A721-B6F6FE91FCCC}']

    //- Read events -//
    function getOnReadStart: TTransStartEvent;
    procedure setOnReadStart( e: TTransStartEvent );
    function getOnReadSuccess: TTransSuccessEvent;
    procedure setOnReadSuccess( e: TTransSuccessEvent );
    function getOnReadFail: TTransFailEvent;
    procedure setOnReadFail( e: TTransFailEvent );

    //- property getter setter -//
    function getBaseURL: string;
    procedure setBaseURL(value: string);
    function getApiName: String;
    procedure setApiName(value: String);
    function getApiKey: String;
    procedure setApiKey(value: String);

    function Read( priority: boolean = false; strIn: string = ''; langOut: string = ''; ATask: TATask_Trans = nil ): string;

    //- Pascal only, properties -//
    property BaseURL_Trans: string read getBaseURL write setBaseURL;
    property ApiName_Trans: String read getApiName write setApiName;
    property ApiKey_Trans: String read getApiKey write setApiKey;

    property OnTransReadStart: TTransStartEvent read getOnReadStart write setOnReadStart;
    property OnTransReadSuccess: TTransSuccessEvent read getOnReadSuccess write setOnReadSuccess;
    property OnTransReadFail: TTransFailEvent read getOnReadFail write setOnReadFail;
  end;

function CreateTrans( BaseURL_Trans, ApiName_Trans, ApiKey_Trans: string): ITransClient;

type
  TTransClient = class( TInterfacedObject, ITransClient)
  private
    fBaseURL_Trans: string;
    fapiName_Trans: string;
    fapiKey_Trans: string;

  private
    fOnReadStart: TTransStartEvent;
    fOnReadSuccess: TTransSuccessEvent;
    fOnReadFail: TTransFailEvent;

  private
    //- Call event handlers
    procedure doReadFail( Err: string; priority: boolean = false );
    procedure doReadSuccess( A: string; priority: boolean = false );
    procedure doReadStart(priority: boolean = false);
    procedure Internal( strIn, langOut: string; ATask: TATask_Trans = nil );

  private //- ITransClient -//
    //- Getters and Setters
    function getOnReadStart: TTransStartEvent;
    procedure setOnReadStart( e: TTransStartEvent );
    function getOnReadSuccess: TTransSuccessEvent;
    procedure setOnReadSuccess( e: TTransSuccessEvent );
    function getOnReadFail: TTransFailEvent;
    procedure setOnReadFail( e: TTransFailEvent );

    //- Property getter and setter -//
    function getBaseURL: string;
    procedure setBaseURL(value: string);
    function getApiName: String;
    procedure setApiName(value: String);
    function getApiKey: String;
    procedure setApiKey(value: String);

  public
    //- Interface methods.
    function Read( priority: boolean = false; strIn: string = ''; langOut: string = ''; ATask: TATask_Trans = nil ): string;
    //function Inse( priority: boolean = false; strIn: string = ''; langOut: string = ''; ATask: TATask_Trans = nil ): string;

  public
    constructor Construct( BaseUrl_Trans, ApiName_Trans, ApiKey_Trans: string ); reintroduce;
    destructor Destroy; override;

    property OnTransReadStart: TTransStartEvent read getOnReadStart write setOnReadStart;
    property OnTransReadSuccess: TTransSuccessEvent read getOnReadSuccess write setOnReadSuccess;
    property OnTransReadFail: TTransFailEvent read getOnReadFail write setOnReadFail;

  end;

var
  query_wait: integer;

implementation

{ function }
function CreateTrans( BaseURL_Trans, ApiName_Trans, ApiKey_Trans: string ): ITransClient;
begin
  query_wait:=0;
  Result := TTransClient.Construct(BaseURL_Trans, ApiName_Trans, ApiKey_Trans);
end;

{ TTransClient }

constructor TTransClient.Construct( BaseURL_Trans, ApiName_Trans, ApiKey_Trans: string );
begin
  inherited Create;
  fBaseURL_Trans := BaseURL_Trans;
  fApiName_Trans := ApiName_Trans;
  fApiKey_Trans := ApiKey_Trans;
end;

destructor TTransClient.Destroy;
begin
  inherited Destroy;
end;

procedure TTransClient.doReadFail( Err: string; priority: boolean = false );
begin

  //if not priority then begin
    if assigned(fOnReadFail) then
      TThread.Queue(TThread.Current,procedure() begin
          fOnReadFail( Self, Err );
      end)

end;

procedure TTransClient.doReadStart(priority: boolean = false);
begin
  //if not priority then begin
    if assigned(fOnReadStart) then
      TThread.Queue(TThread.Current,procedure() begin
        fOnReadStart(self);
      end)

end;

procedure TTransClient.doReadSuccess( A: string; priority: boolean = false );
begin
    if assigned(fOnReadSuccess) then
      TThread.Queue(TThread.Current,procedure() begin
          fOnReadSuccess(self,A);
      end)
end;

procedure TTransClient.Internal( strIn, langOut: string; ATask: TATask_Trans = nil );
var
  body: string;
  strurl, resp: TStringStream;
  fResultsArray, o1: TJSONArray;
  o, o2: TJSONObject;
  resu, Path, error: string;
  T: ITask;
  Http2: TNetHTTPClient;
begin
  //LHeaders := [TNetHeader.Create('content-type', 'application/json'),
  //TNetHeader.Create('Authorization', 'key = ' + cFCMServerKey)];
  {$IFDEF CPU64BITS}
    Path:=Path+'ssl64';
  {$ELSE}
    Path:=Path+'ssl32';
  {$ENDIF}

  HTTP2 := TNetHTTPClient.Create(nil);
  Http2.ContentType:=CONTENTTYPE_APPLICATION_JSON;
  //http2.ConnectionTimeout:=100;
  //http2.ResponseTimeout:=200;

  doReadStart;

  //http1.Request.CustomHeaders.AddValue(apiName, apiKey);
  body:='[{"text":"'+strIn+'"}]';
  strurl:=TStringStream.Create(body, TEncoding.UTF8);
  resp:=TStringStream.Create('', TEncoding.UTF8);
  try
    try
      http2.Post(cbaseurl_Trans+langOut, strurl, resp, [TNetHeader.Create('content-type', 'application/json'), TNetHeader.Create(capiName_Trans, capiKey_Trans)]);
      sleep(100);
    except
      //on E: EIdHTTPProtocolException do begin
        // use E.ErrorCode, E.Message, y E.ErrorMessage ...
        doReadFail('Fallo en conexion.');
        Exit;
      //end;
    end;
    try
      fResultsArray:=((TJSONObject.ParseJSONValue(resp.DataString)) as TJSONArray);
    except
        //doReadFail('Error en traduccion.');

        Exit;
    end;
    o := fResultsArray.Items[0] as TJSONObject;
    o1 := o.GetValue('translations') as TJSONArray;
    o2 := o1.Items[0] as TJSONObject;
    resu:=o2.GetValue('text').Value; //resp.DataString;
    try
      ATask(resu);
      doReadSuccess(resu);
    finally
      //fResultsArray.DisposeOf;
      //FreeAndNil(fResultsArray);
      fResultsArray := nil;
    end;
  finally
    strurl.Free;
    resp.Free;
  end;

end;

function TTransClient.getBaseURL: string;
begin
  Result := fBaseURL_Trans;
end;

function TTransClient.getApiName: string;
begin
  Result := fApiName_Trans;
end;

function TTransClient.getApiKey: string;
begin
  Result := fApiKey_Trans;
end;

function TTransClient.getOnReadFail: TTransFailEvent;
begin
  Result := fOnReadFail;
end;

function TTransClient.getOnReadStart: TTransStartEvent;
begin
  Result := fOnReadStart;
end;

function TTransClient.getOnReadSuccess: TTransSuccessEvent;
begin
  Result := fOnReadSuccess;
end;

procedure TTransClient.setBaseURL(value: string);
begin
  fBaseURL_Trans := value;
end;

procedure TTransClient.setApiName(value: string);
begin
  fApiName_Trans := value;
end;

procedure TTransClient.setApiKey(value: string);
begin
  fApiKey_Trans := value;
end;

procedure TTransClient.setOnReadFail(e: TTransFailEvent);
begin
  fOnReadFail := e;
end;

procedure TTransClient.setOnReadStart(e: TTransStartEvent);
begin
  fOnReadStart := e;
end;

procedure TTransClient.setOnReadSuccess(e: TTransSuccessEvent);
begin
  fOnReadSuccess := e;
end;

function TTransClient.Read( priority: boolean = false; strIn: string = ''; langOut: string = ''; ATask: TATask_Trans = nil ): string;
var
  T: ITask;
begin


  if not priority then begin
    T:=TTask.Create(procedure
    begin
        Internal( strIn, langOut, ATAsk );
    end);
    T.Start;
  end else
    Internal( strIn, langOut, ATAsk );

end;

end.
